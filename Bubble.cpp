#include "Bubble.h"
#include <QGraphicsScene>
#include <QTimer>

Bubble::Bubble(QGraphicsItem *parent): QObject(), QGraphicsPixmapItem(parent) {
    setPixmap(QPixmap(":/images/bubble.gif"));

    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT( move()));

    timer->start(40);
}

void Bubble::move() {
    setPos(x(),y()-10);
    if (pos().y()+46 < 0) {
        scene()->removeItem(this);
        delete this;
    }
}
