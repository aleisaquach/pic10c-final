#ifndef BUBBLE_H
#define BUBBLE_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>

class Bubble:public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    Bubble(QGraphicsItem * parent = 0);
public slots:
    /* move bubbles up screen and delete
     * bubbles once they reach the top */
    void move();
};

#endif // BUBBLE_H
