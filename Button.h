#ifndef BUTTON_H
#define BUTTON_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>

class Button:public QObject, public QGraphicsRectItem {
    Q_OBJECT
public:
    Button(QString name, QGraphicsItem* parent=NULL);

    /* emit clicked signal */
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    /* change color of button when hovered over */
    void hoverEnterEvent(QGraphicsSceneHoverEvent * event);

    /* change color back to original color
     * when not hovered over */
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
signals:
    void clicked();
private:
    QGraphicsTextItem* text;
};

#endif // BUTTON_H
