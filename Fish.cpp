#include "Fish.h"
#include "Game.h"
#include "Food.h"
#include "Bubble.h"
#include "Pollution.h"
#include "Life.h"
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QList>
#include <stdlib.h>
#include <QMediaPlayer>

extern Game * game;

Fish::Fish(QGraphicsItem *parent): QObject(), QGraphicsPixmapItem(parent) {
    setPixmap(QPixmap(":/images/fish.png")); // insert fish

    bubblesSound = new QMediaPlayer();
    bubblesSound->setMedia(QUrl("qrc:/sounds/bubbles.mp3"));

    eatingSound = new QMediaPlayer();
    eatingSound->setMedia(QUrl("qrc:/sounds/eating.mp3"));

    dyingSound = new QMediaPlayer();
    dyingSound->setMedia(QUrl("qrc:/sounds/dying.mp3"));
}

void Fish::keyPressEvent(QKeyEvent *event) {
    // delete colliding items to imitate fish eating
    QList<QGraphicsItem *> eaten = collidingItems();
    for(int i = 0, n = eaten.size(); i < n; ++i) {
        if ( (typeid(*(eaten[i])) == typeid(Food)) || (typeid(*(eaten[i])) == typeid(Pollution))
             || (typeid(*(eaten[i])) == typeid(Life)) ) {
            scene()->removeItem(eaten[i]);

            if (typeid(*(eaten[i])) == typeid(Food)) { // increase game score if food is eaten
                game->score->increase();
            } else if (typeid(*(eaten[i])) == typeid(Pollution)) { // decrease health if pollution is eaten
                game->health->decrease();
            } else if (typeid(*(eaten[i])) == typeid(Life)) { // increase health if life is eaten
                game->health->increase();
            }

            delete eaten[i]; // delete colliding food item

            // play eating sound with every collision
            if (eatingSound->state() == QMediaPlayer::PlayingState) {
                eatingSound->setPosition(0);
            } else if (eatingSound->state() == QMediaPlayer::StoppedState) {
                eatingSound->play();
            }

            // quit game when out of lives, play dying sound
            if (game->health->getHealth() == 0) {
                game->gameOver();
                dyingSound->play();
            }
        }
    }

    // respond to movement
    switch (event->key()) {
        case Qt::Key_Left:
            this->moveLeft();
            break;
        case Qt::Key_Right:
            this->moveRight();
            break;
        case Qt::Key_Up:
            this->moveUp();
            break;
        case Qt::Key_Down:
            this->moveDown();
            break;
    }

    return;
}

void Fish::eat() {
    Food * food = new Food();
    int random = rand() % 700;
    food->setPos(random,0);
    scene()->addItem(food);
}

void Fish::breathe() {
    Bubble * bubble = new Bubble();
    bubble->setPos(x()+100,y());
    scene()->addItem(bubble);

    bubblesSound->play();
}

void Fish::eatBad() {
    Pollution * pollution = new Pollution();
    int random = rand() % 700;
    pollution->setPos(random,0);
    scene()->addItem(pollution);
}

void Fish::eatGood() {
    Life * life = new Life();
    int random = rand() % 700;
    life->setPos(random,0);
    scene()->addItem(life);
}

void Fish::moveLeft() {
    setPos(x()-10,y());
}

void Fish::moveRight() {
    setPos(x()+10,y());
}

void Fish::moveUp() {
    setPos(x(),y()-10);
}

void Fish::moveDown() {
    setPos(x(),y()+10);
}
