#ifndef FISH_H
#define FISH_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QMediaPlayer>

class Fish:public QObject, public QGraphicsPixmapItem {
Q_OBJECT
public:
    Fish(QGraphicsItem * parent=0);

    /* make fish respond to arrow movements
     * delete objects that collide with fish
     * play sound when fish collides with item
     * update health / score depending on item */
    void keyPressEvent(QKeyEvent * event);
public slots:
    /* drop food randomly across screen */
    void eat();

    /* move bubbles up from mouth of fish
     * play sound for bubbles when released */
    void breathe();

    /* drop pollution randomly across screen */
    void eatBad();

    /* drop lives randomly across screen */
    void eatGood();

    /* move fish left */
    void moveLeft();

    /* move fish right */
    void moveRight();

    /* move fish up */
    void moveUp();

    /* move fish down */
    void moveDown();
private:
    QMediaPlayer * bubblesSound;
    QMediaPlayer * eatingSound;
    QMediaPlayer * dyingSound;
};

#endif // FISH_H
