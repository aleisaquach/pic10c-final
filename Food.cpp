#include "Food.h"
#include <QGraphicsScene>
#include <QTimer>

Food::Food(QGraphicsItem *parent): QObject(), QGraphicsPixmapItem(parent) {
    setPixmap(QPixmap(":/images/shrimp.png"));

    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    timer->start(50);
}

void Food::move() {
    setPos(x(),y()+10);
    if (pos().y() > 600) {
        scene()->removeItem(this);
        delete this;
    }
}
