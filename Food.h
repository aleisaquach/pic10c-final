#ifndef FOOD_H
#define FOOD_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>

class Food:public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    Food(QGraphicsItem * parent = 0);
public slots:
    /* move food down screen and
     * delete at bottom of screen*/
    void move();
};

#endif // FOOD_H
