#include "Game.h"
#include "Button.h"
#include <QTimer>
#include <QFont>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <stdlib.h>

Game::Game(QWidget *parent){
    //create scene
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,800,600);
    setBackgroundBrush(QBrush(QImage(":/images/bg.PNG")));

    // make scene to visualize
    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(800,600);

    // play background music
    QMediaPlaylist * play = new QMediaPlaylist();
    play->addMedia(QUrl("qrc:/sounds/bgsound.mp3"));
    play->setPlaybackMode(QMediaPlaylist::Loop);

    QMediaPlayer * music = new QMediaPlayer();
    music->setPlaylist(play);
    music->play();
}

void Game::drawPanel(){
    // draw background
    QGraphicsRectItem* panel = new QGraphicsRectItem(0,0,800,600);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::darkCyan);
    panel->setBrush(brush);
    panel->setOpacity(.5);
    scene->addItem(panel);

    // draw center panel
    QGraphicsRectItem* panel2 = new QGraphicsRectItem(200,150,400,300);
    QBrush brush2;
    brush2.setStyle(Qt::SolidPattern);
    brush2.setColor(Qt::white);
    panel2->setBrush(brush2);
    scene->addItem(panel2);
}

void Game::addButtons(){
    // add play button
    Button* play = new Button(QString("play"));
    play->setPos(320,300);
    scene->addItem(play);
    connect(play,SIGNAL(clicked()),this,SLOT(start()));

    // add quit button
    Button* quit = new Button(QString("quit"));
    quit->setPos(420,300);
    scene->addItem(quit);
    connect(quit,SIGNAL(clicked()),this,SLOT(close()));
}

void Game::menu(){
    // draw center panel
    drawPanel();

    // add instructions
    QGraphicsTextItem* titleText = new QGraphicsTextItem(QString("aquarium"));
    titleText->setPos(330,200);
    titleText->setFont(QFont("helvetica",20));
    titleText->setDefaultTextColor(Qt::darkCyan);
    scene->addItem(titleText);

    QGraphicsTextItem* instrText1 = new QGraphicsTextItem(QString("the point is to eat as many shrimp as possible."));
    instrText1->setPos(255,240);
    instrText1->setFont(QFont("helvetica",10));
    instrText1->setDefaultTextColor(Qt::darkCyan);
    scene->addItem(instrText1);

    QGraphicsTextItem* instrText2 = new QGraphicsTextItem(QString("use the arrow keys to move the fish around!"));
    instrText2->setPos(262,255);
    instrText2->setFont(QFont("helvetica",10));
    instrText2->setDefaultTextColor(Qt::darkCyan);
    scene->addItem(instrText2);

    QGraphicsTextItem* instrText3 = new QGraphicsTextItem(QString("avoid pollution and look out for special shells"));
    instrText3->setPos(260,270);
    instrText3->setFont(QFont("helvetica",10));
    instrText3->setDefaultTextColor(Qt::darkCyan);
    scene->addItem(instrText3);

    // add start and quit buttons
    addButtons();
}

void Game::gameOver() {
    // stop movement of items on screen
    for (size_t i = 0, n = scene->items().size(); i < n; i++) {
        scene->items()[i]->setEnabled(false);
    }

    // draw center panel
    drawPanel();

    // display gameover message
    QGraphicsTextItem* gameOverText = new QGraphicsTextItem(QString("game over"));
    gameOverText->setPos(330,200);
    gameOverText->setFont(QFont("helvetica",20));
    gameOverText->setDefaultTextColor(Qt::darkCyan);
    scene->addItem(gameOverText);

    // add start and quit buttons
    addButtons();
}

void Game::start(){
    scene->clear();

    // create fish
    fish = new Fish();
    int random_x = rand() % 700;
    int random_y = rand() % 500;
    fish->setPos(random_x,random_y);
    fish->setFlag(QGraphicsItem::ItemIsFocusable);
    fish->setFocus();
    scene->addItem(fish);

    // add health
    health = new Health();
    scene->addItem(health);
    score = new Score();
    score->setPos(score->x(),score->y()+20);
    scene->addItem(score);

    // add food
    QTimer * foodTimer = new QTimer();
    QObject::connect(foodTimer,SIGNAL(timeout()),fish,SLOT(eat()));
    foodTimer->start(5000);

    // add bubbles
    QTimer * bubblesTimer = new QTimer();
    QObject::connect(bubblesTimer,SIGNAL(timeout()),fish,SLOT(breathe()));
    bubblesTimer->start(2000);

    // add pollution
    QTimer * pollutionTimer = new QTimer();
    QObject::connect(pollutionTimer,SIGNAL(timeout()),fish,SLOT(eatBad()));
    pollutionTimer->start(2500);

    // add life
    QTimer * lifeTimer = new QTimer();
    QObject::connect(lifeTimer,SIGNAL(timeout()),fish,SLOT(eatGood()));
    lifeTimer->start(15000);

    show();
}
