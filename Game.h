#ifndef GAME_H
#define GAME_H

#include "Fish.h"
#include "Health.h"
#include "Score.h"
#include <QGraphicsView>
#include <QGraphicsScene>

class Game: public QGraphicsView{
    Q_OBJECT
public:
    Game(QWidget * parent=0);

    QGraphicsScene * scene;
    Fish * fish;
    Health * health;
    Score * score;

    /* draw background panel and white
     * panel in the center of screen */
    void drawPanel();

    /* add start and quit buttons to screen */
    void addButtons();

    /* create starting menu with instructions
     * and start / quit buttons underneath */
    void menu();

    /* stop movement of objets on screen
     * add panel with start / quit buttons */
    void gameOver();

public slots:
    /* clear screen then start game */
    void start();
};

#endif // GAME_H
