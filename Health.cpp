#include "Health.h"
#include <QFont>

Health::Health(QGraphicsItem *parent): QGraphicsTextItem(parent) {
    health = 4;

    setPlainText(QString("health: ") + QString::number(health));
    setDefaultTextColor(Qt::darkGreen);
    setFont(QFont("helvetica",10));
}

void Health::increase() {
    health++;
    setPlainText(QString("health: ") + QString::number(health));
}

void Health::decrease() {
    health--;
    setPlainText(QString("health: ") + QString::number(health));
}

int Health::getHealth(){
    return health;
}
