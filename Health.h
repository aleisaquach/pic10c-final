#ifndef HEALTH_H
#define HEALTH_H

#include <QGraphicsTextItem>

class Health: public QGraphicsTextItem {
public:
    Health(QGraphicsItem * parent=0);

    /* increase health, update screen display*/
    void increase();

    /* decrease health, update screen display */
    void decrease();

    /* return health */
    int getHealth();
private:
    int health;
};

#endif // HEALTH_H
