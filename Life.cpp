#include "Life.h"
#include <QGraphicsScene>
#include <QTimer>

Life::Life(QGraphicsItem *parent): QObject(), QGraphicsPixmapItem(parent) {
    setPixmap(QPixmap(":/images/life.png"));

    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    timer->start(25);
}

void Life::move() {
    setPos(x(),y()+10);
    if (pos().y() > 600) {
        scene()->removeItem(this);
        delete this;
    }
}
