#ifndef LIFE_H
#define LIFE_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>

class Life:public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    Life(QGraphicsItem * parent = 0);
public slots:
    /* move life down screen and
     * delete at bottom of screen*/
    void move();
};

#endif // LIFE_H
