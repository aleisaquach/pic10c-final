#include "Pollution.h"
#include <QGraphicsScene>
#include <QTimer>

Pollution::Pollution(QGraphicsItem *parent) {
    setPixmap(QPixmap(":/images/pollution.png"));

    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    timer->start(100);
}

void Pollution::move() {
    setPos(x(),y()+10);
    if (pos().y() > 600) {
        scene()->removeItem(this);
        delete this;
    }
}
