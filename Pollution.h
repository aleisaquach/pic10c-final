#ifndef POLLUTION_H
#define POLLUTION_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>

class Pollution:public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    Pollution(QGraphicsItem * parent = 0);
public slots:
    /* move pollution down screen and
     * delete at bottom of screen*/
    void move();
};

#endif // POLLUTION_H
