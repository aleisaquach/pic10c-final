# aquarium 
aquarium is a game about one small fish in a big blue ocean. the player can control the fish with the arrow keys. the goal is to eat as many shrimp as possible, avoid pollution, and keep a lookout for special shells! 

## video of working game
a simulation of the working game can be found [here](https://drive.google.com/file/d/1W72HsMSOqFC-Ibm2E3hv2nj3maIzknM9/view?usp=sharing)

## process
#### week 5
In week 5, I began to seriously look for final project ideas. Looking at possible C++ projects online made me realize that C++ is actually a very powerful tool if used correctly. Doing small homework assignments for previous classes did not allow me to fully grasp the potential of it.

I realized that you are able to make small games with a nice graphical interface through c++. I looked at several examples including: [a tetris game](https://www.youtube.com/watch?v=zH_omFPqMO4), [a game of chess](https://www.youtube.com/watch?v=_4EuZI8Q8cs), [a neural network simulation](https://www.youtube.com/watch?v=zIkBYwdkuTk), [flappy bird](https://www.youtube.com/watch?v=b6A4XHkTjs8), and [snake](https://www.youtube.com/watch?v=E_-lMZDi7Uw). 

The projects and their interfaces were shocking to me. Before, though I knew that c++ was capable of creating games, as I have created small games for my classes before like tic tac toe or a game of cards, I did not know that it could be used to make a graphical interface as well. 

Though I found a lot of projects interesting, I had a hard time figuring out what I wanted to do. I spent time thinking about a project idea, and I realized that the most interesting part about the projects that I have looked at were their graphical interface. I wanted a project that would lend itself to this. While thinking about this, I realized that I also did not want to ignore the situation of the world around us. I wanted my project to mean something important to me.

Building on this, I decided to create a virtual fish tank that people could interact with to keep them from being lonely during self isolation. To check if this idea was plausible / feasible with c++, I spent time looking samples or tutorials of virtual aquariums online.
I found two projects that were similar in concept ([proj1](https://www.youtube.com/watch?v=B1lCU9ey1W0) and [proj2](https://www.youtube.com/watch?v=x8Bh-NrQFRQ)). But, they did not include any source code or a tutorial. 

Nonetheless, I liked the concept I had, and it seemed possible. I decided that even though I could not find a step-by-step guide of how to create the groundwork for a virtual fish tank, I would do my best to learn the basics and figure it out along the way. If I was able to create a working prototype, I would continue to build it to make it more aesthetically pleasing and possibly add more species of fish. So, I submitted my proposal through email.

Proposal: a virtual fish tank with fishes that will grow after being fed a certain amount of times.

#### week 6
My final project was accepted in week 6, but I had personal matters going on and could not fully focus on the project. 

#### week 7
When I tried to begin my project, I had no idea where to start. So, I found a comprehensible [game tutorial series](https://www.youtube.com/watch?v=8ntEQpg7gck&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV) with Qt on youtube.The tutorial immediately noted what would be required, including basic knowledge of C++ (pointers and memory management) and basic knowledge of Qt (widgets). I did knot know what widgets were and felt like my C++ was a little rusty, so I decided to first find a C++ review tutorial series and a Qt+ tutorial series. 

I began a [C++ tutorial series](https://www.youtube.com/playlist?list=PLfVsf4Bjg79Cu5MYkyJ-u4SyQmMhFeC1C) to refresh on fundamentals. The tutorial series covered the [history of c++](https://www.youtube.com/watch?v=SQHREey_Yuc&list=PLfVsf4Bjg79Cu5MYkyJ-u4SyQmMhFeC1C&index=2&t=0s),[why c++](https://www.youtube.com/watch?v=brqRL_t0RmM&list=PLfVsf4Bjg79Cu5MYkyJ-u4SyQmMhFeC1C&index=2) should be utilized, the conversion of [source code to executable](https://www.youtube.com/watch?v=ZTu0kf-7h08&list=PLfVsf4Bjg79Cu5MYkyJ-u4SyQmMhFeC1C&index=3), the [tool set](https://www.youtube.com/watch?v=SezbHFe-jy4&list=PLfVsf4Bjg79Cu5MYkyJ-u4SyQmMhFeC1C&index=4), and a [hello world program](https://www.youtube.com/watch?v=e840YAaK620&list=PLfVsf4Bjg79Cu5MYkyJ-u4SyQmMhFeC1C&index=6). At this point, I realized that the C++ tutorial series was too simple. I felt like I decently refreshed my memory on c++, and moved onto trying to learn Qt. 

The [Qt tutorial series](https://www.youtube.com/playlist?list=PL2D1942A4688E9D63) covered the basics of the program. The [first video](https://www.youtube.com/watch?v=6KtOzh0StTc&list=PL2D1942A4688E9D63&index=2&t=0s) was an introduction to what Qt programming was. The [second video](https://www.youtube.com/watch?v=Id-sPu_m_hE&list=PL2D1942A4688E9D63&index=2) covered a basic hello world application alongside the details of what the .pro file does. The [third video](https://www.youtube.com/watch?v=GxlB34Cn0zw&list=PL2D1942A4688E9D63&index=3) introduced the GUI application. Through it, I learned how to use the main window and create push buttons. I realized that at the basal level, everything in Qt is an object. The [forth video](https://www.youtube.com/watch?v=JtyCM4BTbYo&list=PL2D1942A4688E9D63&index=4) covered signals and slots through the implementation of sliders. I struggled with this concept and reviewed the video a few times to understand it conceptually. The [fifth video](https://www.youtube.com/watch?v=wUH_gu2HdQE&list=PL2D1942A4688E9D63&index=5) showed how to display new pop up windows. 

#### week 8
At this point, I felt I had a basic understanding of Qt, but I was still struggling to implement it on my own and create a game. I decided to look for a better way of applying the material that I was learning. During this, I found myself back at the [C++ Qt game tutorial](https://www.youtube.com/watch?v=8ntEQpg7gck&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV) that I started with. I figured that this would be a good place to start, as I would learn how to apply the basics of game design on Qt. Hopefully, afterwards, I would figure out which parts of the tutorial would be applicable to my own project and have an idea of where to start when creating the aquarium. 

While watching the tutorial series, I created my own project and tried to follow along. 

The [first video](https://www.youtube.com/watch?v=8ntEQpg7gck&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=1) showed how to draw a player. In this, I learned how to implement the QGraphicsScene, QGraphicsRectItem, and QGraphicsView classes in order to create a scene and place a player into it. This seemed very practical to my own project, as I would be able to use this to create the fish. 

The [second video](https://www.youtube.com/watch?v=kvrAAP_ayWI&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=2) showed us how to connect the player to the arrow keys in order to move the player around as well as how to focus the screen on the player. A possible application of this to my project is in the case that I want to extend my project so that I can move the fish around to fight enemies. 

The [third video](https://www.youtube.com/watch?v=Fgwn_ENPL8c&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=3) covered shooting with the space bar. More importantly though, it covered how to make a class that uses slots and stressed that a class that does this must inherit from QObject and have the macro Q_OBJECT. This would be applicable in creating food pellets on the screen for the fish to eat when the screen is clicked. 

The [forth video](https://www.youtube.com/watch?v=4IAb_YESVY4&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=4) cleaned up the project and showed us how to keep the screen the same size as the view. This would prevent the screen from moving around as the fish moves around. 

The [fifth video](https://www.youtube.com/watch?v=tnPS2t5W_lY&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=5) covered how to add enemies that spawn randomly at different locations. Moreover, it showed that Qt keeps track of colliding objects. This would be especially applicable in making food pellets disappear when the fish collides with them in order to simulate eating. 

The [sixth video](https://www.youtube.com/watch?v=SnkszRZPcvM&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=6) showed how to add the player's health and score. I followed the tutorial for the score portion of the video, but for the health portion I wanted to test myself and see if I could write the code by myself. Because it was so similar to health, I was able to display the health and make the health decrease without following the tutorial. This helped reassure me that I was learning and understanding rather than just copying the tutorial. A possible application of this to my project is having the fish's helath decrease over time if it is not fed and increase when fed. 

The [seventh video](https://www.youtube.com/watch?v=DS7CDpIrwN4&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=7) covered adding sound effects. I had a hard time implementing this tutorial. Everytime I tried to add a sound to the resource file, it would show an error. I later realized that I had to drag the files into the Qt file that I had so that I could access it. When I finally was able to add the file, the sound would not play. I suspected that for some reason Qt would not accept a .m4a. So, I converted the sounds to .mp3 files. By doing so, I was able to make the sound work properly. 

The [eighth video](https://www.youtube.com/watch?v=xPs40BrYHkg&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=8) covered how to add graphics using QPixmap and QImage. I learned how to add graphics for the player, projectiles, and background. This was a lot easier after I learned how to add resource files from the previous tutorial. This was especially applicable to my project, as I finally learned how to add the graphics for my project including the background of the aquarium and the fish. I was especially excited to start my project after this video, as I was looking forward to creating the graphical interface of my game.

At this point, I felt as if I had a decent grasp of how to make a very basic game. Though the series was longer, I decided to stop here and begin implementing it into my own project.

#### week 9
I used the game tutorial series as a bare bones structure for my project. 

To start, I first set up the basic background for my game.
**background screen** I set the window size, turned off scroll bars, and fixed the size. Referencing the game project that I created while following the tutorial made this process a lot easier. I then personalized by creating a resource file to add a background image for the window.
**background music** I found a background sound that I liked, recorded it, and uploaded to my game. At first, the sound would not play, but I remembered from my previous experience trying to add sound while following the tutorial series that Qt creator does not accept .m4a files. So, I double checked the file type of my recorded background sound, and once I realized that it was an .m4a file, I converted it to an .mp3 file. Though I was able to get the background sound to play, it would not loop infinitely. Since the recording was only about 50 seconds, it would end and the rest of the game would be silent. I wanted to fix this, so I looked for ways to loop from QMediaPlayer class. Though I was not able to find a way, I did find a way to create a playlist which had the loop feature. Following [these steps](https://stackoverflow.com/questions/37690616/play-background-music-in-a-loop-qt), I was able to create a playlist, add the background sound to this playlist, and have the playlist loop in the background infinitely. 

I then created the fish for my aquarium. 
**player** I created a fish class and added a resource file for a picture of the fish and added this graphic to the fish. At this point, I realized that my project idea of a virtual aquarium was a little boring. Instead of creating a fish tank where the fish swim back and forth on their own, I wanted to create a game where the user could control the fish and eat objects. Previously, the idea of creating a game where the user could control the player was daunting to me, but after watching the game tutorial series, I realized that it was not as hard to implement as I had previously thought. So, I decided to switch my project idea.
**new project proposal** game where the user could control the fish by using the arrow keys. by pressing on the arrow keys, the fish would move back and forth. the goal is to eat objects. objects that the fish collides with would disappear and effectively be eaten. 
**adding controls** I added key press events to the fish. To do so, I referenced back to the [tutorial series](https://www.youtube.com/watch?v=kvrAAP_ayWI&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=2).

I added food for the fish to eat. 
**food** To do this, I created a food class and added a resource file for a picture of the shrimp that the fish would eat and added this graphic to the food.
**food move** I wanted the food to drop at a random location on the top of the screen then float down, as if it were sinking. That way, the player could control the fish and try to eat them as they sink. I realized that implementing this would be similar to spawning enemies in the [tutorial series](https://www.youtube.com/watch?v=tnPS2t5W_lY&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=5).
**fish eating food** I wanted the fish to look as if it were eating the food. In order to do this, the food that the fish touched would have to disappear. In order to implement this, I used the [tutorial series](https://www.youtube.com/watch?v=tnPS2t5W_lY&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=5) to make colliding objects disappear. 
**fish eating sound** Finally to cement the visual that the fish was eating the food, I added a sound effect that was tied the deletion of the food items that were eaten. 

I wanted the game to be more interesting than the player just controlling the fish to eat food, so I decided to add random objects that drop that the fish should not eat. 
**adding pollution** To make this relevant to the outside world, I decided to add pollution to the game. These would act as objects that the fish should not eat and act as obstacles for the player. The implementation of this was similar to creating food. However, I changed the timer that set when the pollution would be released so that they would drop more often and make the game more difficult. Moreover, I made the timer for their movement slower so that they would stay visible on the screen for longer. 

#### week 10
With the new development of the game, I thought it would be applicable to add a score bar for the fish as well as a health bar.
**score bar** I wanted the score bar that would increase with every food/shrimp that was eaten to motivate the player to keep playing. I was able to do this by referencing back to the [game tutorial series](https://www.youtube.com/watch?v=SnkszRZPcvM) which covered how to display score and health on the screen. 
**health bar** I used a similar implementation as the score bar to create a health bar in order to create a consequence for whenever pollution was eaten. With every pollution that the fish ate, health would decrease. 
**quit at gameover** With the addition of a health bar, I thought it would be important for the game to stop when health reached 0. Otherwise, it didn’t make sense for the game to continue with negative health. In order to figure out how to do this, I looked online and found this [sample code](https://stackoverflow.com/questions/35276849/how-to-exit-a-game-in-qt-when-health-is-0). However, when I tried to follow this logic, my code kept crashing. I realized that it was linked to the quit() slot and that I forgot to add Q_MACRO to my class. When I added Q_MACRO, it still crashed. After looking up the error message, I found a few [forums](https://stackoverflow.com/questions/9928238/unresolved-external-symbol-in-object-files) and looked up [why the macro was causing an error](https://stackoverflow.com/questions/9929269/c-qt-q-object-macro-causes-an-error). Eventually, I realized that I forgot to clean the project and run qmake. After doing so, my code worked fine, and I was able to quit the game after health reached 0. 
**adding a life** Because there was now a health bar, I wanted to further build my project so that there was a way for the fish to increase their health. To do this, I added a special food drop item, a shell. I made the timer for the drops longer and the speed of the movement faster to make them rare and difficult to get. The implementation of this was similar to the food and pollution classes. I then connected the health bar to it being eaten as well. 

I found that the game quitting after health reached 0 to be slightly abrupt and instead wanted to create a game over scenario that would display "game over" as well as the option restart or quit the game.
**game over scenario** With the game now able to quit when health reaches 0, I wanted there to be an option to restart the game or quit after game over. To do so, I watched [game over tutorials](https://www.youtube.com/watch?v=TmOr1pMdKJs&list=PLMgDVIa0Pg8WrI9WmZR09xAbfXyfkqKWy&index=35) of how to implement a game over scenario. Watching this confused me, as I wasn’t sure how to make the screen pop up or how to apply the buttons. So, I looked up tutorials on [how to build the pop-up screen](https://www.youtube.com/watch?v=lLrhonSCV4s) as well as how to [create buttons](https://www.youtube.com/watch?v=o_Ozdyz73Bw). After watching these, I was able to successfully combine the concepts to create a game over pop-up screen that displayed “game over” as well as buttons with the option to either restart the game or to quit. 

Finally, I wanted to add a menu screen to the beginning of my game.
**menu** 
Initially, I thought about implementing a temporary pop-up screen that would disappear after a certain amount of time. During this, I found tutorials on how to [make splash screen](https://www.youtube.com/watch?v=r85MOIW-Ktw) and looked at the [QTimer class](https://doc.qt.io/qt-5/qtimer.html). However, after implementing a splash screen, I realized that it was not exactly what I had in mind. Instead, I thought it would be better to have a pop-up screen similar to the game over screen that instead displayed instructions as well as an option to start or quit the game. I was initially confused on how to implement this, but after watching a [tutorial video](https://www.youtube.com/watch?v=lLrhonSCV4s) I was able to get a general idea of how to create this flow.

#### finals week
I spent most of finals week **cleaning up my project**. This included changing the movement controls from if-then statements to switch statements and adding comments. 

I then created a **readme file**. I honestly had no idea where to begin with this, so I found a [f](https://www.makeareadme.com/)[e](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)[w](https://guides.co/g/bitbucket-101/11158) descriptions on how to start a readme, what they were, and how to add them to bitbucket. Of these, I found [this](https://www.makeareadme.com/) to be the most helpful. 

#### synposis & future direction
I am pretty proud of myself for being able to create a working game. Though it is not nearly as fancy as I dreamed of making it, I still cannot believe that I went from not knowing that Qt existed or that we could use C++ to create an interface to building a game that I am proud of. I hope to continue to add to this class in the future and possibly make the fish look like it is swimming. 

## connetions to class
#### RAII
Qt utilizes RAII to handle the reallocation of resources. This is done by linking resources to finite scope-based objects. 

#### Q_OBJECT
In class, we learned how to use Q_OBJECT and to implement signals and slots. I used this throughout my project to create an interactive interface that responds to clicks (for buttons) as well as arrow key presses (movement of fish). They were also used to link the dropping of shrimp, pollution, and shells to a timer and to play music. I also created custom slots to connect the clicking of butons to the start of the game. 

#### keyPressEvents
In class, we learned how to connect the dots to keyPressEvents using a switch. I updated my game to reflect this. Rather than using if-then statements to handle the movement of the fish. 

#### QPushButton
In class, we learned about the QPushButton class. Intead of utilizing the class, I implemented my own button class in order to have more control over them. 

#### sprite
In discussion, we learned how to create a sprite animation. Though I was not able to get around to adding this to my project, I hope to in the future to make it appear as if the fish is swimming and to make the game more dynamic / interesting. 

#### readme.md
In class, the importance of a readme.md file was stressed. In this process, I incorporated a readme file to help others understand what this project is about. 

#### GIT version control 
In class, we learned how to use git version control as well as its potential. Not only does it keep track of our work but it allows us to revert back to any point in time, create experimental branches, and merge branches together. Nonetheless, while building this project, I personally did not feel the need to use it. Though I did not really version control while working on this project, I understand its potential and have used it for other projects.

#### standard template library
In class, we learned about the different types of containers, such as sequence containers that provides access to elements sequentially, container adaptors which rely on underlying containers to handle the elements, associative containers which implement ordered associative arrays, and unordered associative containers that implement has tables. Moreover, we learned about the different properties each of these have that provide pros and cons for specific situations. Though I did not feel the need to use these in my current project, I can see the potential of using them in the future if I were to continue to build my project.


## files included
| file | description |
| [aquarium.pro](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Aquarium.pro) | Qt creator uses to keep track of project |
| [aquariu.pro.user](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Aquarium.pro.user) | user specific .pro file |
| [bubble.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Bubble.cpp) | declarations of bubble member functions | 
| [bubble.h](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Bubble.h) | creates bubbles that float from fish's mouth to simulate breathing |
| [button.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Button.cpp) | declarations of button member functions |
| [button.h](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Button.h) | custom button class | 
| [fish.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Fish.cpp) | declaration of fish member functions |
| [fish.h](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Fish.h) | creates the fish |
| [food.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Food.cpp) | declaration of food member functions | 
| [food.h](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Food.h) | drops shrimp randomly across the screen |
| [game.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Game.cpp) | declaration of game member functions | 
| [game.h](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Game.h) | handles the creation of the game | 
| [health.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Health.cpp) | declaration of health member functions | 
| [health.h](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Health.h) | creates health bar for fish |
| [life.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Life.cpp) | declaration of life member functions |
| [life.h](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Life.h) | drops shells (that act as lives) randomly across the screen 
| [pollution.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Pollution.cpp) | declaration of pollution member functions | 
| [pollution.h](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Pollution.h) | drops polution randomly across the screen | 
| [readme.md](https://bitbucket.org/aleisaquach/pic10c-final/src/master/README.md) | mark down readme file |
| [score.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Score.cpp) | declaration of score member functions |
| [score.h](https://bitbucket.org/aleisaquach/pic10c-final/src/master/Score.h) | creates score bar that increases with every shrimp eaten |
| [bg.png](https://bitbucket.org/aleisaquach/pic10c-final/src/master/bg.PNG) | background image |
| [bgsound.mp3](https://bitbucket.org/aleisaquach/pic10c-final/src/master/bgsound.mp3) | background sound |
| [bubble.gif](https://bitbucket.org/aleisaquach/pic10c-final/src/master/bubble.gif) | bubble image |
| [bubbles.mp3](https://bitbucket.org/aleisaquach/pic10c-final/src/master/bubbles.mp3) | bubbles sound |
| [dying.mp3](https://bitbucket.org/aleisaquach/pic10c-final/src/master/dying.mp3) | dying sound | 
| [eating.mp3](https://bitbucket.org/aleisaquach/pic10c-final/src/master/eating.mp3) | eating sound |
| [fish.png](https://bitbucket.org/aleisaquach/pic10c-final/src/master/fish.png) | fish graphic |
| [life.png](https://bitbucket.org/aleisaquach/pic10c-final/src/master/life.png) | shell image (for life) | 
| [main.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/main.cpp) | main file | 
| [mainwindow.cpp](https://bitbucket.org/aleisaquach/pic10c-final/src/master/mainwindow.cpp) | mainwindow | 
| [pollution.png](https://bitbucket.org/aleisaquach/pic10c-final/src/master/pollution.png) | bottle image (for pollution) |
| [res.qrc](https://bitbucket.org/aleisaquach/pic10c-final/src/master/res.qrc) | resource files | 
| [shrimp.png](https://bitbucket.org/aleisaquach/pic10c-final/src/master/shrimp.png) | shrimp image (for food) |