#include "Score.h"
#include <QFont>

Score::Score(QGraphicsItem *parent): QGraphicsTextItem(parent) {
    score = 0;

    setPlainText(QString("score: ") + QString::number(score));
    setDefaultTextColor(Qt::darkCyan);
    setFont(QFont("helvetica",10));
}

void Score::increase() {
    score++;
    setPlainText(QString("score: ") + QString::number(score));
}

int Score::getScore(){
    return score;
}
